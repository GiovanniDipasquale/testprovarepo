/**
 * Created by roman.rosiak on 07.12.2016.
 */

public class ITA_IFM_Annulment_Constants {


    public static final String BPM_REQUEST_URL      = 'https://ostcoe.enelenergia.it:6443/CRMT/mercato/libero/service/presentationsfdc/asyncannullamentoportendpoint';
    //Annulment method type
    public static final String VERTICALE_ANNULMENT  = 'ANNULLAMENTOSALESFORCE';
    public static final String BPM_ANNULMENT        = 'ANNULLAMENTOINTEGRATO';
//    public static final String VERTICALE_ANNULMENT = 'ENDMOGE';
//    public static final String BPM_ANNULMENT = 'ENDMOGE';

    //Object code
    public static final String CASE_RECORD_CODE                     = '500';
    public static final String CASE_ITEM_RECORD_CODE                = 'a2p';
    public static final String ORDER_ITEM_RECORD_CODE               = 'a1p';
    public static final String QUOTE_RECORD_CODE                    = 'a21';

    //Object Name
    public static final String OBJECT_CASE_NAME                     = 'Case';
    public static final String OBJECT_CASE_ITEM_NAME                = 'ITA_IFM_Case_Items__c';
    public static final String OBJECT_CONFIGURATION_ITEM_NAME       = 'NE__OrderItem__c';
    public static final String OBJECT_QUOTE_NAME                    = 'NE__Quote__c';

    //Result
    public static final String RESULT_OK                            = 'OK';
    public static final String RESULT_KO                            = 'KO';
    public static final String VERTICALE_ANNULMENT_RESULTCODE_OK    = 'ANN_100';
    public static final String VERTICALE_ANNULMENT_DESCRIPTION_OK   = 'OK Annullamento Salesforce';
    public static final String VERTICALE_ANNULMENT_RESULTCODE_KO    = 'ANN_101';
    public static final String VERTICALE_ANNULMENT_DESCRIPTION_KO   = 'KO Annullamento Salesforce';

    public static final String BPM_ANNULMENT_RESULTCODE_OK          = 'ANN_200';
    public static final String BPM_ANNULMENT_RESULTCODE_KO          = 'ANN_201';
    public static final String BPM_ANNULMENT_DESCRIPTION_OK         = 'OK Annullamento BPM';
    public static final String BPM_ANNULMENT_DESCRIPTION_KO         = 'KO Annullamento BPM';

    public static final String ORDER_ANNULMENT_RESULTCODE_OK        = 'ANN_300';
    public static final String ORDER_ANNULMENT_DESCRIPTION_OK       = 'Order Management';
    public static final String ORDER_ANNULMENT_RESULTCODE_KO        = 'ANN_301';
    public static final String ORDER_ANNULMENT_DESCRIPTION_KO       = 'Order Management execution not possible';

    public static final String PHASE_RESULTCODE_KO                  = 'ANN_999';
    public static final String PHASE_DESCRIPTION_KO                 = 'Annullamento non ammissibile';

    public static final String BPM_REQUEST_SENT_CODE_OK             = 'ANN_777';
    public static final String BPM_REQUEST_SENT_DESCRIPTION_OK      = 'OK Request has been sent';
    public static final String BPM_REQUEST_NOT_SENT_CODE_KO         = 'ANN_888';
    public static final String BPM_REQUEST_NOT_SENT_DESCRIPTION_KO  = 'KO Request has not been sent';

    //Annulment values
    public static final String STATUS_CHIUSO                        = 'Chiuso';
    public static final String STATUS_ANNULLATO                     = 'Annullato';
    public static final String STATUS_ANNULLAMENTO                  = 'ANNULLAMENTO';
    public static final String STATUS_ANNULLATA                     = 'Annullata';
    public static final String SUBSTATUS_OFFER_ANNULLATA            = 'Offer - Annullata';

    //Annulamento Stauts pick list values
    public static final String ANNULAMENTO_IN_PROGRESS              = 'Annullamento in corso';
    public static final String ANNULAMENTO_IN_PROGRESS_BPM          = 'Annullamento BPM in corso';
    public static final String ANNULAMENTO_ORDER_MANAGEMENT         = 'Annullamento Order Management';
    public static final String ANNULAMENTO_COMPLETE                 = 'Annullato';

    public static final String ANNULAMENTO_KO                       = 'Annullamento KO';

    // Annulment Queue object values of the Status field
    public static final String AQ_STATUS_DA_ANNULLARE               = 'Da annullare';
    public static final String AQ_STATUS_ANNULLATO                  = 'Annullato';
    public static final String AQ_STATUS_PRESO_IN_CARICO            = 'Preso in carico';
    public static final String AQ_STATUS_KO                         = 'KO';

    //Annulment WS inbound
    public static final String CODICE_ESITIO_00000                  = 'CRMT-00000';
    public static final String CODICE_ESITIO_00100                  = 'CRMT-00100';
    public static final String CODICE_ESITIO_00200                  = 'CRMT-00200';

    public static final String DESCRIZIONE_ESITIO_00100             = 'Errore generico';
    public static final String DESCRIZIONE_ESITIO_00200             = 'Dati non conformi';

    public static final List<String> SISTEMA_CHIAMANTE_AVAILABLE_VALUES_LIST    = new List<String>{'MIND'};
    public static final List<String> TIPO_ESITO_AVAILABLE_VALUES_LIST           = new List<String>{'1','2','3','5'};
    public static final List<String> ESITO_AVAILABLE_VALUES_LIST                = new List<String>{'OK','KO','RIL','RILAVORAZIONE'};

    public static final String PUT_INTEGRATION_LOG_ACTION_TYPE  = 'AsyncAnnulment';

    public static final String SISTEMA_CHIAMANTE_R2D                = 'R2D';
    public static final String  TIPO_ESITO_5                        = '5';
    public static final String ANNULMENT_STATE_OK                   = 'OK';
    public static final String  ANNULMENT_STATE_KO                  = 'KO';
    public static final String ESITO_OK                             = 'OK';
    public static final String ESITO_KO                             = 'KO';
    public static final String ESITO_RIL                            = 'RIL';
    public static final String DESCRIPTION_OK                       = 'OK';
    public static final String ESITO_RILAVORAZIONE                  = 'RILAVORAZIONE';

    public static final String ESITO_DESCRIPTION_PRESO_IN_CARICO    = 'Preso in carico';

    //Fulfillment Actions
    public static final String FULFILLMENT_ANNULLAMENTO             = 'ANNULLAMENTO';
    public static final String VSA_PROCESS_NAME                     = 'VOLTURA SENZA ACCOLLO';
    public static final String COMMODITY_RECORD_TYPE                = 'Commodity';
    //public static final String FULFILLMENT_NOACTION = 'NO ACTION';
    public static final String FULFILLMENT_NOACTION                 = 'NO AZIONE';

    public static final String PROCESSNAME_ALLACIO                  = 'ALLACCIO'; //ENEL-673 Lukasz Knap <lukasz.knap@accenture.com> 17.10.2017
    public static final Set<String> PROCESS_NAMES_WITH_EMPTY_BPMID  = new Set<String>{'SUBENTRO', 'PRIMA ATTIVAZIONE', 'Prima Attivazione', 'ALLACCIO_E_ATTIVAZIONE'}; //ENEL-727, lukasz.knap@accenture.com, 19.10.2017
    public static final Set<String> PROCESS_NAMES_WITH_BLOCKING_CONFITEM_ANNULMENT = new Set<String>{'SUBENTRO', 'PRIMA ATTIVAZIONE', 'Prima Attivazione', 'ALLACCIO_E_ATTIVAZIONE', 'SWITCH ATTIVO', 'VOLTURA CON ACCOLLO', 'VOLTURA SENZA ACCOLLO', 'RICONTRATTUALIZZAZIONE'}; //ENEL-728, adam.redlinski@accenture.com, 23.10.2017, Start
    public static final String CONFITEM_CHIUSA_DA_CONFERMARE        = 'Chiusa da confermare';
    public static final String CONFITEM_BILLINGPROF_PAYMENT         = 'RID';
    public static final String CONFITEM_BILLINGPROF_PAYMENT_NEW     = 'SDD';    //ENEL-728, adam.redlinski@accenture.com, 23.10.2017, End
    public static final String PROCESS_NAME_ATTIVAZIONE_ALLACCIO    = 'ALLACCIO_E_ATTIVAZIONE';
    public static final String OI_ANNULMENTSTATE_VERTICAL           = 'vertical';
    public static final String OI_ANNULMENTSTATE_IN_PROGRESS        = 'Annullamento Order Management in progress';
    public static final String CANCELLATION_REASON_AUTOMATICO       = 'Annullamento Automatico'; //ENEL-922, d.modrzejewska@accenture.com, 21.12.2017
    public static final String SR_STATUS_WORKING                    = 'Working';

    //ENEL-1149, d.modrzejewska@accenture.com, 20.02.2018, PROCESS_NAMES_WITH_WAITING_OM contains processes in which OM is invoked after all OK from R2D
    public static final Set<String> PROCESS_NAMES_WITH_WAITING_OM   = new Set<String>{ 'SUBENTRO', 'PRIMA ATTIVAZIONE','ALLACCIO E ATTIVAZIONE' }; // MC 19/03/2018 def 21695, 21810

    //NG - Add Insert 07032018
    public static final String PHASE_END                            = 'END';

    //ENEL-1224 michal.bajdek@accenture.com 15.03.2018
    public static final String SWA_RECORDTYPE_FOR_CASE              = 'ITA_IFM_SWA';
    public static final Set<String> UNSUPPORTED_VALUES_FOR_SWA      = new Set<String>{'Ripensamento','DMS - Disdetta Standard'};

    public static final Set<String> CASE_SUBJECT_NO_END             = new  Set<String>{'VARIAZIONE IND FATTURAZIONE','VARIAZIONE IND FORNITURA', 'RECLAMO VERBALE','Informativa', 'Rettifica Fatturazione', 'Rettifica Doppia Fatturazione', 'RECLAMO SCRITTO', 'VARIAZIONE ANAGRAFICA','RESIDENTE','Modifica Potenza'};                                       //NG - Add insert 12042018 - Set processes to not set phase end on case
    //NG - Start modify 05062018 - id 677 NRT - Add VARIAZIONE ANAGRAFICA
    // [BEGIN GS, 2018/07/09, INC000015490752: added RESIDENTE]
    public static final Set<String> CASE_SUBJECT_NO_SKIPALL         = new  Set<String>{'RECLAMO VERBALE','Informativa', 'Rettifica Fatturazione', 'Rettifica Doppia Fatturazione', 'RECLAMO SCRITTO','VARIAZIONE ANAGRAFICA','RESIDENTE'};  //NG - Add insert 12042018 - Set processes to update without skip
    //NG - End modify 05062018
    public static final Set<String> QUOTE_REASON_BBL                = new  Set<String>{'SWITCH ATTIVO'};    //NG - Add insert 13042018 - Set Activation Reason of quote for BBL
    //[BEGIN NG, 2018/05/03, INC000014082539]
    public static final Set<String> OI_CONTRACTUAL_AGREEMENT        = new  Set<String>{'RICONTRATTUALIZZAZIONE','RICONTRATTUALIZZAZIONE SALV','RICONTRATTUALIZZAZIONE FUI','RICONTRATTUALIZZAZIONE DEFAULT','RICONTRATTUALIZZAZIONE TUTELA'}; //NG- Start Insert 27042017 - Set process to manage Contractual Agreement
    //[END NG, 2018/05/03, INC000014082539]
    //NG - Start Insert 02062018 - Defect 822
    //NG - Start Insert 17072018 - Add ITA_IFM_Verifica_GDM_GM - Defect 2140
    //NG - Start Insert 26072017 - Add 'ITA_IFM_Change_Address', 'ITA_IFM_Service_address_change' - Defect 1589
    /**START** @giampaolo.mei@webresults.it-@Modification date  03/08/2018 12:05-@description: aggiunto record type subentro **START**/
    public static final Set<String> CASEITEM_RT_NO_SKIPALL          = new  Set<String>{'ITA_IFM_DISALIMENTABILITA', 'ITA_IFM_Verifica_GDM_GM', 'ITA_IFM_Change_Address', 'ITA_IFM_Service_address_change', 'ITA_IFM_SUBENTRO'};
    /**END**   @giampaolo.mei@webresults.it-@Modification date  03/08/2018 12:05 **END**/
    //NG - End Insert 26072017 - Defect 1589
    //NG - End Insert 17072018 -  Defect 2140
    //NG - End Insert 02062018 - Defect 822

    //NG - Start Insert 02082018 -  Defect 1589
    public static final Set<String> CASEITEM_RT_NO_END              = new  Set<String>{'ITA_IFM_Change_Address', 'ITA_IFM_Service_address_change','ITA_IFM_MODIFICA_POTENZA'};
    //NG - END Insert 02082018

}